#pragma once

#include <string>
#include <vector>
#include <set>
#include <iostream>
#include <cmath>

template <class T>
class BSNode
{
public:
	BSNode(T data);
	BSNode(T* data, int size);
	BSNode(const BSNode& other);

	~BSNode();
	
	void insert(T value);
	BSNode& operator=(const BSNode& other);

	bool isLeaf() const;
	T getData() const;
	BSNode* getLeft() const;
	BSNode* getRight() const;

	bool search(T val) const;

	int getHeight() const;
	int getDepth(const BSNode& root) const;

	std::vector<T> getAllData() const;
	void printNodes() const; //for question 1 part C
protected:
	T _data;
	int _count;
	BSNode* _left;
	BSNode* _right;
};

template<class T>
BSNode<T>::BSNode(T data) {
	this->_data = data;
	this->_count = 1;
	this->_left = nullptr;
	this->_right = nullptr;
}

template<class T>
inline BSNode<T>::BSNode(T* data, int size)
{
	this->_data = data[0];
	this->_count = 1;
	this->_left = nullptr;
	this->_right = nullptr;
	for (int i = 1; i < size; i++)
	{
		this->insert(data[i]);
	}
}

template<class T>
BSNode<T>::BSNode(const BSNode& other) : _data(other._data), _count(other._count) {
	this->_left = other._left;
	this->_right = other._right;
}

template<class T>
BSNode<T>::~BSNode()
{
	delete this->_left;
	delete this->_right;
}

template<class T>
void BSNode<T>::insert(T value)
{
	if (this->_data == value) this->_count++;
	if (value >= this->_data) {
		if (this->_right == nullptr) this->_right = new BSNode(value);
		else this->_right->insert(value);
	}
	else if (value < this->_data) {
		if (this->_left == nullptr) this->_left = new BSNode(value);
		else this->_left->insert(value);
	}
	return;
}

template<class T>
BSNode<T>& BSNode<T>::operator=(const BSNode& other)
{
	if (this != &other) {
		this->_data = other._data;
		this->_left = other._left;
		this->_right = other._right;
	}
	return *this;
}

template<class T>
bool BSNode<T>::isLeaf() const
{
	return this->_left == nullptr && this->_right == nullptr;
}

template<class T>
T BSNode<T>::getData() const
{
	return this->_data;
}


template<class T>
BSNode<T>* BSNode<T>::getLeft() const
{
	return this->_left;
}

template<class T>
BSNode<T>* BSNode<T>::getRight() const
{
	return this->_right;
}

template<class T>
bool BSNode<T>::search(T val) const
{
	if (this->_data == val) return true;
	if (val >= this->_data) {
		if (this->_right == nullptr) return false;
		return this->_right->search(val);
	}
	else if (val < this->_data) {
		if (this->_left == nullptr) return false;
		return this->_left->search(val);
	}
	return false;
}

template<class T>
int BSNode<T>::getHeight() const
{
	if (this->isLeaf()) return 0;

	int lDepth = 0;
	int rDepth = 0;
	if (this->_left != nullptr)  lDepth = this->_left->getHeight();
	if (this->_right != nullptr)  rDepth = this->_right->getHeight();

	return std::max(lDepth, rDepth) + 1;
}

template<class T>
int BSNode<T>::getDepth(const BSNode& root) const
{
	if (!(root.search(this->_data))) return -1;
	return root.getHeight() - this->getHeight();
}

template<class T>
std::vector<T> BSNode<T>::getAllData() const
{
	std::vector<T> allData;
	allData.push_back(this->_data);

	if (this->isLeaf()) {
		return allData;
	}
	if (this->_left != nullptr) {
		std::vector<T> subAllData = _left->getAllData();
		allData.insert(allData.end(), subAllData.begin(), subAllData.end());
	}
	if (this->_right != nullptr) {
		std::vector<T> subAllData = _right->getAllData();
		allData.insert(allData.end(), subAllData.begin(), subAllData.end());
	}

	return allData;
}

template<class T>
void BSNode<T>::printNodes() const
{
	std::vector<T> allData = this->getAllData();
	std::set<T> setAllData(allData.begin(), allData.end());

	for (T elem : setAllData) {
		std::cout << elem << " - " << std::count(allData.begin(), allData.end(), elem) << std::endl;
	}
}
