#pragma once
#include "BSNode.h"

template <class T>
class AVLNode : public BSNode<T> {
	int getBalance() const;
	void insert(T value);
};

template<class T>
inline int AVLNode<T>::getBalance() const
{
	int lHeight = -1;
	int rHeight = -1;
	if (this->_left != nullptr) lHeight = this->_left->getHeight();
	if (this->_right != nullptr) rHeight = this->_right->getHeight();

	return lHeight - rHeight;
}

template<class T>
void AVLNode<T>::insert(T value)
{
	if (this->_data == value) return;
	if (value >= this->_data) {
		if (this->_right == nullptr) this->_right = new BSNode<T>(value);
		else this->_right->insert(value);
	}
	else if (value < this->_data) {
		if (this->_left == nullptr) this->_left = new BSNode<T>(value);
		else this->_left->insert(value);
	}
	return;
}
